﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookwhereBehaviour : MonoBehaviour
{
    [SerializeField] GameObject _combinedObjectPlaceHolder;
    [SerializeField] GameObject _particleIndicator;
    [SerializeField] Animator _animatorCutting1;
    [SerializeField] Animator _animatorCutting2;
    [SerializeField] bool _ButcherTable;
    [SerializeField] Recipes[] CookableRecipes;
    [SerializeField] Transform _UITransform;
    [SerializeField] CookWhereIndicator _indicator;
    [SerializeField] GameObject _arrowIndicator;
    [SerializeField] GameObject _createRecipeIndicator, _putInProductIndicator;

    Transform playerTransform;
    private GameObject eatable;
    List<Eatables> _eatablesInPot = new List<Eatables>();
    List<GameObject> _UIVisuals = new List<GameObject>();
    public bool IsMaking = false;
    bool _isPickupable;
    public bool IsMakeable = false;

    int previoutEatablesInPotAmount = 0;

    Recipes selectedRecipe;

    private void Start()
    {        
        DisableIndicator();
        foreach (Recipes item in CookableRecipes)
        {
            item.Initialize();
        }
        if(_particleIndicator != null) _particleIndicator.SetActive(false);
    }

    public void ShowIndicator(GameObject go)
    {
        Eatables goEatable = null;
        if (go != null)
        {
            ProductBehaviour goProductBeh = go?.GetComponent<ProductBehaviour>();
            goEatable = goProductBeh?.EatableType;
        }

        if (PlayerPrefs.GetInt("Indicator") == 1)
        {
            _arrowIndicator.SetActive(true);

            for (int i = 0; i < CookableRecipes.Length; i++)
            {
                if (goEatable != null)
                {
                    if (go != null && CookableRecipes[i].IsEatableInRecipe(goEatable, _eatablesInPot)) { _putInProductIndicator.SetActive(true); }
                }
                if (_isPickupable) _putInProductIndicator.SetActive(true);
            }

            foreach (var item in CookableRecipes)
            {
                if (item.IsMakeable && item.EatablesNeeded.Length == _eatablesInPot.Count && !_isPickupable && !IsMaking) { _createRecipeIndicator.SetActive(true); break; }
                else _createRecipeIndicator.SetActive(false);
            }
        }
    }

    public void DisableIndicator()
    {
        _arrowIndicator.SetActive(false);
        _putInProductIndicator.SetActive(false);
        _createRecipeIndicator.SetActive(false);
    }
 
    public void AddProduct(GameObject go)
    {
        ProductBehaviour goProductBeh = go?.GetComponent<ProductBehaviour>();
        Eatables goEatable = goProductBeh?.EatableType;

        for (int i = 0; i < CookableRecipes.Length; i++)
        {
            IsMakeable = false;

            if (!CookableRecipes[i].IsEatableInRecipe(goEatable, _eatablesInPot)) { IsMakeable = false; } //if the recipe isn't possible to make then remove it from the recipe list
            else //else add the eatable in the pot 
            {
                IsMakeable = true;
                _eatablesInPot.Add(goEatable);
                break;
            }
        }

        if (_eatablesInPot.Count > previoutEatablesInPotAmount)
        {
            previoutEatablesInPotAmount++;
            _indicator.CreateIndicator(_eatablesInPot[_eatablesInPot.Count - 1].Image);
        }

        foreach (var item in CookableRecipes)
        {
            item.CheckRecipe(_eatablesInPot);
        }
    }

    public void CreateRecipe()
    {
        if (!IsMaking)
        {
            for (int i = 0; i < CookableRecipes.Length; i++)
            {                
                if (CookableRecipes[i].IsMakeable) { IsMaking = true; selectedRecipe = CookableRecipes[i]; break; } // if one of the recipes is makeable then begin to cook
            }
        }
        if (IsMaking) StartCoroutine(Cook()); // start cooking
    }

    public void ResetCookwhere()
    {
        if (_indicator != null)
            _indicator.ClearIndicators();

        _eatablesInPot.Clear();
        IsMaking = false;
        _isPickupable = false;
        selectedRecipe = null;
        previoutEatablesInPotAmount = 0;

        foreach(GameObject gameObject in _UIVisuals)
        {
            Destroy(gameObject);
        }

        foreach (var item in CookableRecipes)
        {
            item.IsMakeable = false;
        }

        _UIVisuals.Clear();
    }

    public void PickUpProduct(Transform _transform, string tag)
    {
        playerTransform = _transform;

        if (_isPickupable)
        {
            _combinedObjectPlaceHolder.GetComponent<ProductBehaviour>().EatableType = selectedRecipe.ReturnEatable;
            _combinedObjectPlaceHolder.GetComponent<ProductBehaviour>().PlayerTag = tag;
            GameObject go = Instantiate(_combinedObjectPlaceHolder, playerTransform);
            go.transform.SetParent(playerTransform);
            go.transform.position = playerTransform.position;
            go.transform.rotation = playerTransform.rotation;
            go.transform.localScale /= 2.5f;

            ResetCookwhere();
        }
    }

    IEnumerator Cook()
    {
        if (!_ButcherTable)
        {
            if (_particleIndicator != null)
            {
                _particleIndicator.SetActive(true);
                FindObjectOfType<AudioManager>().Play("FireSound");
            }

            yield return new WaitForSeconds(2f);

            _isPickupable = true;

            if (_particleIndicator != null)
            {
                FindObjectOfType<AudioManager>().Stop("FireSound");
                _particleIndicator.SetActive(false);
            }
        }
        else
        {
            if (_animatorCutting1 != null && _animatorCutting1 != null)
            {
                FindObjectOfType<AudioManager>().Play("CutSound");
                _animatorCutting1.SetBool("IsCutting", true);
                _animatorCutting2.SetBool("IsCutting", true);
            }
            yield return new WaitForSeconds(2f);

            _isPickupable = true;

            if (_animatorCutting1 != null && _animatorCutting1 != null)
            {
                FindObjectOfType<AudioManager>().Stop("CutSound");
                _animatorCutting1.SetBool("IsCutting", false);
                _animatorCutting2.SetBool("IsCutting", false);
            }
        }
       
    }    
}