﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageNumbersBehavior : MonoBehaviour
{
    [SerializeField]
    private TextMesh textMesh;

    private Color _color;

    // Start is called before the first frame update
    public static DamageNumbersBehavior Create(Transform parent, float damageAmount,Transform damageNumberPrefab)
    {
        Transform DamageNumberTransform = Instantiate(damageNumberPrefab, parent);
        DamageNumberTransform.position = new Vector3(DamageNumberTransform.position.x, DamageNumberTransform.position.y + 1.5f, DamageNumberTransform.position.z);

        DamageNumbersBehavior damageNumber = DamageNumberTransform.GetComponent<DamageNumbersBehavior>();
        damageNumber.Setup(damageAmount);

        return damageNumber;
    }
    public void Setup(float damage)
    {
        textMesh.text = damage.ToString();
        if (damage <= 500)
        {
            textMesh.color = Color.green;
        }
        if (damage > 500 && damage <= 1500)
        {
            textMesh.color = Color.yellow;
        }
        if (damage > 1500)
        {
            textMesh.color = Color.red;
        }
        _color = textMesh.color;

    }

    // Update is called once per frame
    void Update()
    {

        _color.a -= Time.deltaTime / 3;
        textMesh.color = _color;
    }
}
