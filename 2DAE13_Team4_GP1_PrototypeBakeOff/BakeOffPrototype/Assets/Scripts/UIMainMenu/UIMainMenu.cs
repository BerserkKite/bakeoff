﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour
{
    public static string Level;
    public void LoadScene1()
    {
        Level = "Level";
        SceneManager.LoadScene("Character Select");

    }
    public void LoadScene2()
    {
        Level = "Level2";
        SceneManager.LoadScene("Character Select");

    }
    public void LoadScene3()
    {
        Level = "Level3";
        SceneManager.LoadScene("Character Select");

    }
    public void LoadScene4()
    {
        Level = "Level4";
        SceneManager.LoadScene("Character Select");

    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void LoadTutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void LoadProductOverview()
    {
        SceneManager.LoadScene("ProductOverview");
    }
}
