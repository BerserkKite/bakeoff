﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class SlideShowImage : MonoBehaviour
{
    [SerializeField] Sprite[] _images;
    Image image;

    // Start is called before the first frame update
    void Start()
    {
       image = GetComponent<Image>();
        StartCoroutine(WaitTimeBetweenImages());
    }
    IEnumerator WaitTimeBetweenImages()
    {
        for (int i = 0; i < _images.Length; i++)
        {
           
            image.sprite = _images[i];
          
            yield return new WaitForSeconds(10);
           
        }
        StartCoroutine(WaitTimeBetweenImages());

    }
}
