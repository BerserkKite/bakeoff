﻿using UnityEngine;

public class CheckOutOffBounds : MonoBehaviour
{
    public void OutOffBounds(GameObject go)
    {
        if (go.GetComponent<PlayerBehaviour>() == null) return;
        else
        {
            PlayerBehaviour Pbeh = go.GetComponent<PlayerBehaviour>();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<SpawnPlayers>().GetFurthestSpawnPointFromPlayers(Pbeh);
        }
    }
}
