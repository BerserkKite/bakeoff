﻿using UnityEngine;

public class BoundsCheck : MonoBehaviour
{
    GameObject parent;
    private void Start()
    {
        parent = transform.root.gameObject;
    }
    private void OnTriggerEnter(Collider other)
    {
        parent.GetComponent<CheckOutOffBounds>().OutOffBounds(other.gameObject);
    }
}
