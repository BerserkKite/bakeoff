﻿using UnityEngine;

public class PlayertableBehaviour : MonoBehaviour
{
    GameObject[] _childObjects;

    private void Start()
    {
        _childObjects = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            _childObjects[i] = transform.GetChild(i).gameObject;
        }
    }

    public void Interact(GameObject objectToHold, string tag, Transform playerTransform)
    {
        GameObject closestGO = null;
        GetClosest(ref closestGO, ref _childObjects, playerTransform);

        if (closestGO == null) return;
        else
        {
            ItemPlaceholderBehaviour IPbeh = closestGO.GetComponent<ItemPlaceholderBehaviour>();
            if (!IPbeh.HasObject)
            {
                IPbeh.playerTag = tag;
                objectToHold.transform.parent = closestGO.transform;
                objectToHold.transform.position = closestGO.transform.position;
                objectToHold.transform.rotation = closestGO.transform.rotation;
            }
        }
    }

    private void GetClosest(ref GameObject closestGo, ref GameObject[] list, Transform playerTransform)
    {
        float distance = 3f;
        for (int i = 0; i < list.Length; i++)
        {
            if (Vector3.Distance(playerTransform.position, list[i].transform.position) < distance)
            {
                closestGo = list[i];
                distance = Vector3.Distance(playerTransform.position, list[i].transform.position);
            }
        }
    }

    public void ShowInidcator(Transform playerTransform, GameObject go)
    {
        GameObject closestGO = null;
        GetClosest(ref closestGO, ref _childObjects, playerTransform);

        if (closestGO == null || go == null) return;
        else
        {
            closestGO.GetComponent<ItemPlaceholderBehaviour>().ShowIndicator();
        }
    }

    public void DisableIndicator()
    {
        foreach (var item in _childObjects)
        {
            item.GetComponent<ItemPlaceholderBehaviour>().DisableIndicator();
        }
    }
}
