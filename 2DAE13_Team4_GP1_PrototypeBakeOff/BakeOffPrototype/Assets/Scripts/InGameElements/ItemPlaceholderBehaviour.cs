﻿using UnityEngine;

public class ItemPlaceholderBehaviour : MonoBehaviour
{
    public bool HasObject;
    public string playerTag;
    [SerializeField] GameObject _indicator;

    private void Start()
    {
        _indicator.SetActive(false);
    }
    void Update()
    {
        if (transform.childCount > 0)
            HasObject = true;
        else
        {
            HasObject = false;
            playerTag = null;
        }
    }

    public void ShowIndicator()
    {
        if(!HasObject)
            _indicator.SetActive(true);
    }

    public void DisableIndicator()
    {
        _indicator.SetActive(false);
    }
}
