﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarouselBehavior : MonoBehaviour
{
    [SerializeField] private float _angle;
    [SerializeField] private float _timeBeforeCarouselStart;
    private float _timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        transform.Rotate(Vector3.up, _angle);
        if (_timer > _timeBeforeCarouselStart)
        {
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
