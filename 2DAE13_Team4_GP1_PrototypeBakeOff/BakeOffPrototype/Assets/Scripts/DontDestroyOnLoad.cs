﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class DontDestroyOnLoad : MonoBehaviour
{
    AudioSource audio;
    [SerializeField] AudioClip[] MainMenuClips;
    [SerializeField] AudioClip[] Level1Clips;
    [SerializeField] AudioClip[] Level2Clips;
    [SerializeField] AudioClip[] Level3Clips;
    [SerializeField] AudioClip[] Level4Clips;

    [SerializeField] Animator anim;

    string currentSceneName;
    float maxAudioVolume = 0;

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("BackgroundMusic");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        audio = GetComponent<AudioSource>();
        maxAudioVolume = 0.15f;
    }

    private void Update()
    {
        CheckScene();
        CheckMusicTransition();
        CheckSoundVolume();
    }

    void CheckSoundVolume()
    {
        audio.volume = maxAudioVolume * PlayerPrefs.GetFloat("SliderValue");
    }

    private void CheckMusicTransition()
    {
        if (currentSceneName == "Level" || currentSceneName == "Level2" || currentSceneName == "Level3" || currentSceneName == "Level4")
        {
            switch (currentSceneName)
            {
                case "Level":
                    if (audio.clip != Level1Clips[0])
                    {
                        FadeOutAudio(Level1Clips);
                    }
                    break;
                case "Level2":
                    if (audio.clip != Level2Clips[0])
                    {
                        FadeOutAudio(Level2Clips);
                    }
                    break;
                case "Level3":
                    if (audio.clip != Level3Clips[0])
                    {
                        FadeOutAudio(Level3Clips);
                    }
                    break;
                case "Level4":
                    if (audio.clip != Level4Clips[0])
                    {
                        FadeOutAudio(Level4Clips);
                    }
                    break;
            }
        }
        else if(currentSceneName != "Loading Screen")
        {
            if (audio.clip != MainMenuClips[0])
            {
                FadeOutAudio(MainMenuClips);
            }
        }
        else
        {
            audio.Stop();
        }
    }

    private void FadeOutAudio(AudioClip[] clips)
    {
        anim.SetTrigger("FadeOut");
        StartCoroutine(FadeOut(clips));
    }

    private void FadeInAudio()
    {
        anim.SetTrigger("FadeIn");
        
    }

    private void CheckScene()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
    }

    IEnumerator FadeOut(AudioClip[] clips)
    {
        yield return new WaitForSeconds(.7f);
        audio.Stop();
        audio.clip = clips[0];
        audio.Play();
        FadeInAudio();
    }
}
