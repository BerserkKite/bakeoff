﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SCR_PlayVideo : MonoBehaviour
{
    [SerializeField]private RawImage _rawImage;
    [SerializeField]private VideoPlayer _videoPlayer;
    [SerializeField] private GameObject _menu, _loadScreen;

    void Start()
    {
        StartCoroutine(playVideo());
    }

    IEnumerator playVideo()
    {
        _videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!_videoPlayer.isPrepared)
        {
            _menu.SetActive(false);
            yield return waitForSeconds;
            break;
        }
        _menu.SetActive(true);
        _loadScreen.SetActive(false);
        _rawImage.texture = _videoPlayer.texture;
        _videoPlayer.Play();
    }
}
