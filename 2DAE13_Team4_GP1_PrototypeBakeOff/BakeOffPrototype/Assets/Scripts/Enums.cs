﻿public enum ThrowMechanicNames
{
    NormalThrow,
    NinjaThrow,
    RapidThrow,
    Melee,
    ExplodigThrow
}

public enum ProductNames
{
    None,
    Cabbage,
    Tomato,
    Chicken,
    Steak,
    SteakSalad,
    TomatoSlices,
    TomatoSalad
}

public enum GameStages
{
    RoundPlaying,
    RoundEnd,
    GameBegin,
    Pauze
}