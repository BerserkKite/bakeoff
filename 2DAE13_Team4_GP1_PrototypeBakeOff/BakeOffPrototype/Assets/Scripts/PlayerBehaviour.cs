﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(CharacterController))]
public class PlayerBehaviour : MonoBehaviour
{
    CharacterController _controller;

    [SerializeField] GameObject _damageNumberPrefab;
    [SerializeField] Transform _instantiationPoint;
    [SerializeField] float _rotationSpeed = 0.2f;
    [SerializeField] private Configuration _engineConfiguration = null;
    [Range(1,4)][SerializeField] int _playerNumber; public int Playernumber { get => _playerNumber; set => _playerNumber = value; }
    public Animator _animator;
    [SerializeField] float _trow1Delay = 1f;
    public Sprite PlayerSprite;
    List<GameObject> _vegetablesInArea = new List<GameObject>();
    List<GameObject> _cookWhereInArea = new List<GameObject>();
    List<GameObject> _playerTablesInArea = new List<GameObject>();

    public bool HasObject;
    public bool HasSpawnPoint;
    public Transform InitializePosition;
    public float DamageDone, DamageTaken; public int RoundsWon;

    private GameObject _heldObject;
    private ProductBehaviour _heldObjectBehaviour_P;
    private PlayerEngine _engine;
    private string horizontalAxis, verticalAxis, aButton, bButton, xButton, yButton;

    bool _canThrow = true;

    private float _fallTime = 0.3f;
    [SerializeField] private LineRenderer _throwingArc;
    [SerializeField] private Image _endPoint;
    private float radianAngle;
    private float velocity = 8.5f;
    private float angle = 25f;
    private int positions = 12;
    private float maxDistance;

    Vector3 impact = Vector3.zero; Vector3 moveDirection;

    #region MovementSystemVariables
    Vector3 normal = Vector3.up;
    Vector3 ground = Vector3.zero;

    Vector3 characterCenter;
    Vector2 leftStick;
    float characterRayCastHeight;
    float horizontalLeft;
    float verticalLeft;

    LayerMask groundLayer;
    float radius;
    #endregion

    public void ResetPlayer()
    {
        DamageDone = 0;
        DamageTaken = 0;
    }

    public void Respawn(Transform t, bool takeDmg)
    {
        _engine.Respawn();
        _engine.Commit();
        _controller.enabled = false;
        if (takeDmg)
        {
            DamageTaken += 1500;
        }
        StartCoroutine(WaitForEndOfFrame(t, takeDmg));
    }

    IEnumerator WaitForEndOfFrame(Transform t, bool takeDmg)
    {
        yield return new WaitForEndOfFrame();

        if (takeDmg)
        {
            DamageNumbersBehavior.Create(this.transform, 1500, _damageNumberPrefab.transform);
        }
        transform.position = t.position + (Vector3.up * 2);
        _controller.enabled = true;
    }

    // Start is called before the first frame update
    void Start()
    {        
        transform.position = InitializePosition.position;
        transform.parent = null;
        _controller = GetComponent<CharacterController>();
        _engine = new PlayerEngine(_controller, transform, _engineConfiguration, _animator);

        horizontalAxis = "J" + _playerNumber + "HorizontalAxis";
        verticalAxis = "J" + _playerNumber + "VerticalAxis";
        aButton = "J" + _playerNumber + "AButton";
        bButton = "J" + _playerNumber + "BButton";
        xButton = "J" + _playerNumber + "XButton";
        yButton = "J" + _playerNumber + "YButton";

        characterRayCastHeight = _controller.height / 2 + _controller.skinWidth * 2;
        groundLayer = LayerMask.GetMask("Walkable");
        radius = _controller.radius * 0.99f;
    }

    private void FixedUpdate()
    {
        if (GameManager.GameStage == GameStages.RoundPlaying) PlayerMovement();
        ApplyKnockBack();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameStage == GameStages.RoundPlaying)
        {
            CheckHits();
            CheckAbleToPickUp();
            CheckAbleToInteract();
            CreateIndicators();

            if (Input.GetButton(bButton) && HasObject && _canThrow)
            {
                _throwingArc.enabled = true;
                _endPoint.enabled = true;
                RenderArc();
            }

            if (Input.GetButtonUp(bButton) && HasObject && _canThrow && _heldObject != null)
            {
                FindObjectOfType<AudioManager>().Play("ThrowSound");
                _throwingArc.enabled = false;
                _endPoint.enabled = false;
                _canThrow = false;
                _animator.SetBool("IsTrow1", true);
                _heldObjectBehaviour_P = _heldObject?.GetComponent<ProductBehaviour>();
                _heldObjectBehaviour_P?.ThrowProduct(transform.forward, _heldObjectBehaviour_P.EatableType, this.tag, transform.rotation);
                StartCoroutine(WaitForSec(_trow1Delay));
                StartCoroutine(WaitForThrow(0.8f));
            }

            if (_instantiationPoint.childCount != 0)
            {
                _heldObject = _instantiationPoint.GetChild(0).gameObject;
                _heldObjectBehaviour_P = _heldObject?.GetComponent<ProductBehaviour>();
                _heldObjectBehaviour_P.PlayerTag = this.tag;
                HasObject = true;
            }
            else
            {
                HasObject = false;
                _heldObject = null;
                _throwingArc.enabled = false;
                _endPoint.enabled = false;
            }

            _cookWhereInArea.Clear();
            _vegetablesInArea.Clear();
            _playerTablesInArea.Clear();
        }
    }

    private void RenderArc()
    {
        _throwingArc.positionCount = positions;
        _throwingArc.SetPositions(CalculateArcArray());
    }

    private Vector3[] CalculateArcArray()
    {
        velocity = _heldObjectBehaviour_P.EatableType.ForceAmount + 0.5f;
        Vector3[] arcArray = new Vector3[positions];
        radianAngle = Mathf.Deg2Rad * angle;
        maxDistance = (velocity * velocity * (Mathf.Sin(2 * radianAngle))) / Physics.gravity.magnitude;
        for (int i = 0; i < positions; i++)
        {
            float t = (float)i / (positions / 1.5f);
            arcArray[i] = CalculateArcPoint(t, maxDistance);
        }
        _endPoint.rectTransform.localPosition = new Vector3(0, 0.01f, arcArray[11].z - 0.5f);
        return arcArray;
    }

    private Vector3 CalculateArcPoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float y = 1.5f + (x * Mathf.Tan(radianAngle)) - ((Physics.gravity.magnitude * x * x) /
                                            (2 * (velocity * Mathf.Cos(radianAngle)) * (velocity *
                                             Mathf.Cos(radianAngle))));
        return new Vector3(0, y, x);
    }

    private void ApplyKnockBack()
    {
        if (impact.magnitude > 0.2f) _engine.Impact(impact);
        impact = Vector3.Slerp(impact, Vector3.zero, 5 * Time.deltaTime);
    }

    IEnumerator WaitForThrow(float delay)
    {
        yield return new WaitForSeconds(delay);
        _canThrow = true;
    }

    IEnumerator WaitForSec(float delay)
    {      
        yield return new WaitForSeconds(delay);
        _animator.SetBool("IsTrow1", false);

        yield return new WaitForSeconds(0.5f);
        HasObject = false; _heldObject = null;
    }

    private void PlayerMovement()
    {
        if (_canThrow)
        {
            _engine.Begin();
            //Determine the player surface
            normal = Vector3.up;
            ground = Vector3.zero;

            characterCenter = transform.position + _controller.center;

            //Determine point of floor contact
            RaycastHit hitInfo;
            Ray groundPointRay = new Ray(characterCenter, Vector3.down);
            if (Physics.SphereCast(groundPointRay, radius, out hitInfo, characterRayCastHeight, groundLayer))
            {
                ground = hitInfo.point;
            }

            //Determine orientation of surface at floor contact
            Ray groundNormalRay = new Ray(new Vector3(ground.x, characterCenter.y, ground.z), Vector3.down);
            if (Physics.Raycast(groundNormalRay, out hitInfo, characterRayCastHeight))
            {
                normal = hitInfo.normal;
            }

            //Get left stick input
            horizontalLeft = Input.GetAxis(horizontalAxis);
            verticalLeft = Input.GetAxis(verticalAxis);

            leftStick = new Vector2(horizontalLeft, verticalLeft);
            leftStick = leftStick.sqrMagnitude > 2 ? leftStick.normalized : leftStick;

            moveDirection = new Vector3(leftStick.x, 0, leftStick.y);

            //_velocity = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
            
            if (!Mathf.Approximately(moveDirection.sqrMagnitude, 0.0f))
            {
                ApplyRotation(moveDirection);
            }

            if (_controller.isGrounded)
            {
                _engine.Ground(normal);
                _engine.Movement(normal, moveDirection);

            }
            _engine.Commit();
        }
    }
    
    void ApplyRotation(Vector3 worldDirection)
    {
        //Quaternion worldOrientation = Quaternion.Slerp(_controller.transform.rotation, Quaternion.LookRotation(worldDirection, Vector3.up),_rotationSpeed);
        //_controller.transform.rotation = worldOrientation;
        Vector3 lookAtPosition = transform.position + worldDirection;
        Vector3 rotation = transform.rotation.eulerAngles;
        transform.LookAt(lookAtPosition);
        transform.rotation = Quaternion.Slerp(Quaternion.Euler(rotation), transform.rotation,_rotationSpeed);
    }

    private void CreateIndicators()
    {
        for (int i = 0; i < _vegetablesInArea.Count; i++)
        {
            ReturnToBasicMaterial(i);

            if (Vector3.Distance(_vegetablesInArea[i].transform.position, transform.position) < 1.5f)
            {
                GameObject closestObj = null;
                GetClosest(ref closestObj, ref _vegetablesInArea);

                ProductBehaviour pb = closestObj.GetComponent<ProductBehaviour>();
                pb.ChangeToHighlightMaterial(_heldObject);
                if (pb.IsThrowen) pb.ChangeToNormalMaterial();
            }
            else
                ReturnToBasicMaterial(i);
        }

        if (_heldObject != null)
            _heldObject.GetComponent<ProductBehaviour>().ChangeToNormalMaterial();

        for (int i = 0; i < _cookWhereInArea.Count; i++)
        {
            DisableCookwhereIndicator(i);

            if (Vector3.Distance(_cookWhereInArea[i].transform.position, transform.position) < 1.5f)
            {
                GameObject closestObj = null;
                GetClosest(ref closestObj, ref _cookWhereInArea);

                CookwhereBehaviour cb = closestObj.GetComponent<CookwhereBehaviour>();
                cb.ShowIndicator(_heldObject);                
            }
            else
                DisableCookwhereIndicator(i);
        }

        for (int i = 0; i < _playerTablesInArea.Count; i++)
        {
            if (_playerTablesInArea.Count == 0) break;

            DisablePlayerTableIndicator(i);

            if (Vector3.Distance(_playerTablesInArea[i].transform.position, transform.position) < 1.5f)
            {
                _playerTablesInArea[i].GetComponent<PlayertableBehaviour>().ShowInidcator(transform, _heldObject);
            }
            else
                DisablePlayerTableIndicator(i);
        }
    }

    private void DisablePlayerTableIndicator(int i)
    {
        _playerTablesInArea[i].GetComponent<PlayertableBehaviour>().DisableIndicator();
    }

    private void DisableCookwhereIndicator(int i)
    {
        _cookWhereInArea[i].GetComponent<CookwhereBehaviour>().DisableIndicator();
    }

    private void ReturnToBasicMaterial(int i)
    {
        _vegetablesInArea[i].GetComponent<ProductBehaviour>().ChangeToNormalMaterial();
    }

    private void GetClosest(ref GameObject closestGo, ref List<GameObject> list)
    {
        float distance = 3f;
        for (int i = 0; i < list.Count; i++)
        {
            if (Vector3.Distance(transform.position, list[i].transform.position) < distance)
            {
                closestGo = list[i];
                distance = Vector3.Distance(transform.position, list[i].transform.position);
            }
        }
    }

    private void CheckAbleToPickUp()
    {
        if (Input.GetButtonDown(aButton) && !_animator.GetBool("IsTrow1") && _canThrow)
        {
            CookwhereBehaviour behaviour = GetClosestCookwhere()?.GetComponent<CookwhereBehaviour>();

            if (_heldObject == null)
            {
                if (behaviour != null)
                {
                    behaviour.PickUpProduct(_instantiationPoint, tag);
                }

                if (_instantiationPoint.childCount == 0 && !CheckIsOnPlayertable())
                    _heldObject = GetClosestVegetable();
                else if (_instantiationPoint.childCount == 0 && CheckIfIsOwnTable())
                    _heldObject = GetClosestVegetable();
            }

            else
            {
                CheckCookWhere(behaviour);

                for (int i = 0; i < _playerTablesInArea.Count; i++)
                {
                    PlayertableBehaviour PTbeh = _playerTablesInArea[i].GetComponent<PlayertableBehaviour>();
                    if (_instantiationPoint.childCount == 0) break;

                    PTbeh.Interact(_heldObject, tag, transform);
                }

                if(_heldObject != null)
                {
                    _heldObject.GetComponent<ProductBehaviour>().Release();
                    _heldObject = null;
                    HasObject = false;
                }
            }
        }
    }

    private void CheckCookWhere(CookwhereBehaviour behaviour)
    {
        if (behaviour == null) return;

        behaviour.AddProduct(_heldObject);

        if (behaviour.IsMakeable)
        {
            Destroy(_heldObject);
            _heldObject = null;
            HasObject = false;
        }
    }

    private bool CheckIfIsOwnTable()
    {
        GameObject closestObj = null;
        GetClosest(ref closestObj, ref _vegetablesInArea);

        ItemPlaceholderBehaviour PTbeh = closestObj?.transform.parent.GetComponent<ItemPlaceholderBehaviour>();
        if (PTbeh == null) return false;

        if (PTbeh.playerTag == tag) return true;
        else return false;
    }

    private bool CheckIsOnPlayertable()
    {
        GameObject closestObj = null;
        GetClosest(ref closestObj, ref _vegetablesInArea);

        ItemPlaceholderBehaviour PTbeh = closestObj?.transform?.parent?.GetComponent<ItemPlaceholderBehaviour>();

        if (PTbeh == null) return false;
        else return true;
    }

    private GameObject GetClosestVegetable()
    {
        GameObject closestGo = null;

        GetClosest(ref closestGo, ref _vegetablesInArea);
        
        if (closestGo != null)
        {
            closestGo.transform.position = _instantiationPoint.position;
            closestGo.transform.SetParent(_instantiationPoint);

            HasObject = true;
            _vegetablesInArea.Remove(closestGo);
        }
        return closestGo;
    }

    private GameObject GetClosestCookwhere()
    {
        GameObject closestGo = null;

        GetClosest(ref closestGo, ref _cookWhereInArea);
                
        return closestGo;
    }

    private void CheckAbleToInteract()
    {
        if (Input.GetButtonDown(xButton))
        {
            CookwhereBehaviour behaviour = GetClosestCookwhere()?.GetComponent<CookwhereBehaviour>();
            if (behaviour != null && !behaviour.IsMaking)
                behaviour.CreateRecipe();
        }
    }
    
    private void CheckHits()
    {
        Collider[] hitObjects = Physics.OverlapSphere(transform.position + Vector3.up, 1.5f);
        int i = 0;
        while (i < hitObjects.Length)
        {
            if (hitObjects[i].gameObject.CompareTag("Product") && hitObjects[i].gameObject.transform.parent != _instantiationPoint)
                _vegetablesInArea.Add(hitObjects[i].gameObject);
            if (hitObjects[i].gameObject.CompareTag("CookWhere") && hitObjects[i].gameObject.transform.parent != _instantiationPoint)
                _cookWhereInArea.Add(hitObjects[i].gameObject);
            if (hitObjects[i].gameObject.CompareTag("PlayerTable") && hitObjects[i].gameObject.transform.parent != _instantiationPoint)
                _playerTablesInArea.Add(hitObjects[i].gameObject);            
            i++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Product") && collision.collider?.GetComponent<ProductBehaviour>().PlayerTag != tag)
        {
            ProductBehaviour productBeh = collision.collider.GetComponent<ProductBehaviour>();
            FindObjectOfType<AudioManager>().Play("HitSound");
            DamageTaken += productBeh.EatableType.DamageAmount;

            Vector3 direction = collision.transform.forward;
            direction.Normalize();

            direction.y = 0;
            impact += direction * productBeh.EatableType.KnockbackForce;
        }
        
    } 

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawWireSphere(transform.position + Vector3.up, 1.5f);
    //}
    
}