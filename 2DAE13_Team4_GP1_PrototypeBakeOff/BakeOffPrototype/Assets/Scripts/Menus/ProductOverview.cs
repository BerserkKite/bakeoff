﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProductOverview : MonoBehaviour
{
    [SerializeField] Eatables[] eatables;
    [SerializeField] Dropdown dropDown;
    [SerializeField] GameObject UiButton;
    #region StatsVariables

    [SerializeField] Text Damage, ThrowDistance, ThrowSort, Name;

    #endregion

    List<Sprite> spriteList = new List<Sprite>();
    List<Eatables> eatableList = new List<Eatables>();
    SortByName sortByName = new SortByName();

   

    private void Start()
    {
        eatableList = eatables.ToList();
        eatableList.Sort(sortByName); //sort the input list by name

        foreach (var item in eatableList)
        {
            //if(item.Image.sprite != null)
            //    spriteList.Add(item.Image.sprite);
            GameObject prefab = Instantiate(UiButton);
            if (item.Image.sprite != null)
                prefab.GetComponent<Image>().sprite = item.Image.sprite;
                
        }

        dropDown.AddOptions(spriteList);

        ChangeStats(0);
    }

    public void IndexChanged(int index)
    {
        ChangeStats(index);
    }

    private void ChangeStats(int index)
    {
        Damage.text = eatableList[index].DamageAmount.ToString();
        ThrowDistance.text = eatableList[index].ForceAmount.ToString();
        ThrowSort.text = eatableList[index].ThrowMechanic.ToString();
        Name.text = eatableList[index].EatableName;
    }
}
