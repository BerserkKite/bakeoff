﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    [SerializeField]
    private Text text;
    private string[] Recipes = new string[6] 
    {
        "Cook the chicken", 
        "Cook the steak", 
        "Cut the tomato", 
        "Cut the tomato and the cabbage", 
        "Cut the tomato and the cabbage, Cook the chicken and throw in a pot",
        "Cut the tomato and bake the steak, throw in a pot and add a cabbage"
    };

    string randomRecipe;
    float _timer;
    bool started=false;

    // Start is called before the first frame update
    void Start()
    {
        randomRecipe = Recipes[Random.Range(0, 6)];
        text.text = randomRecipe;
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > 5 && !started)
        {
            started = true;
            StartCoroutine(LoadAsyncOpperation());
        }
    }

    IEnumerator LoadAsyncOpperation()
    {
        AsyncOperation GameLevel = SceneManager.LoadSceneAsync(UIMainMenu.Level);
            yield return new WaitForEndOfFrame();
    }
}
