﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingMat : MonoBehaviour
{
    [SerializeField] private float _xMovement;
    [SerializeField] private float _yMovement;

    // Update is called once per frame
    void Update()
    {
        float _xOffset = Time.time * _xMovement;
        float _yOffset = Time.time * _yMovement;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(_xOffset, _yOffset);
        
    }
}
