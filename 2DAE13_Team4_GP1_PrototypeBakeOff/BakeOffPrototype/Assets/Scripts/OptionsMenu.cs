﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    bool IsGore = true;
    bool IsIndicator = true;
    [SerializeField] Slider slider;

    void Start()
    {
        PlayerPrefs.SetFloat("SliderValue", slider.value);
        SetGore();
        SetIndicator();
    }

    void SetGore()
    {
        if (IsGore)
            PlayerPrefs.SetInt("Gore", 1);
        else
            PlayerPrefs.SetInt("Gore", 0);
    }

    void SetIndicator()
    {
        if (IsIndicator)
            PlayerPrefs.SetInt("Indicator", 1);
        else
            PlayerPrefs.SetInt("Indicator", 0);
    }

    public void Gore()
    {
        IsGore = !IsGore;
        SetGore();
    }
    public void Indicators()
    {
        IsIndicator = !IsIndicator;
        SetIndicator();
    }

    void Update()
    {
        PlayerPrefs.SetFloat("SliderValue", slider.value);
    }
}
