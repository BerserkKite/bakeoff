﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class SetRound : MonoBehaviour
{
    [SerializeField] PauzeBehaviour pauzeBeh;
    [SerializeField] int _roundSeconds = 150, _roundsToPlay = 5;
    [SerializeField] int _waitUntilNewRound = 5, _waitUntilStartScreen = 10;
    [SerializeField] Transform _pointerTransform;
    [SerializeField] Image _fillBar, _roundTally;
    [SerializeField] GameObject _endOfRoundPanel, _winnerPanel, _counterTextObj, _beginroundTextObj, _bottomBorder;
    [SerializeField] Image winnerSprite;
    [SerializeField] Sprite[] _tallySprites;
    [SerializeField] TextMeshProUGUI _counterText, _beginroundCountertext;
    [SerializeField] GameObject Fireworks;

    float degreesToAdd;
    float fillAmountToSubtract;
    int roundNumber;
    bool startRound = false;

    [SerializeField] GetPlayerStats playerStats;
    [SerializeField] ShowPlayerStats inRoundPlayerStats;
    GameObject[] allCookwheres;
    ProductSpawn[] productSpawns;

    private void Start()
    {
        pauzeBeh.enabled = false;
        _winnerPanel.SetActive(false);
        _endOfRoundPanel.SetActive(false);
        fillAmountToSubtract = 1 / (float)_roundSeconds;
        degreesToAdd = 360 * fillAmountToSubtract;

        allCookwheres = GameObject.FindGameObjectsWithTag("CookWhere");
        productSpawns = FindObjectsOfType<ProductSpawn>();
        playerStats.Initialize();
        FindObjectOfType<AudioManager>().Play("CountDownSound");
        StartRound();
        Fireworks.SetActive(false);
    }

    private void Update()
    {
        if(startRound)
            UpdateTime();
    }

    public void StartRound()
    {
        _counterTextObj.SetActive(false);
        _endOfRoundPanel.SetActive(false);
        _fillBar.fillAmount = 1;

        StartCoroutine(FirstroundCountdown());      
    }

    IEnumerator FirstroundCountdown()
    {
        yield return new WaitForEndOfFrame();
        Resets();
      
        if (roundNumber == 0)
        {
            GameManager.GameStage = GameStages.GameBegin;
            _beginroundTextObj.SetActive(true);
            for (int i = _waitUntilNewRound; i > 0; i--)
            {
                
                _beginroundCountertext.text = i.ToString();
                yield return new WaitForSeconds(1);
            }
            _beginroundTextObj.SetActive(false);
        }

        GameManager.GameStage = GameStages.RoundPlaying;
        startRound = true;
    }

    float currentTime = 0;
    float previousTime = 0;

    void UpdateTime()
    {
        pauzeBeh.enabled = true;
        inRoundPlayerStats.ShowStats();

        if (GameManager.GameStage == GameStages.RoundPlaying)
        {
            if (currentTime < _roundSeconds)
            {
                if (currentTime >= previousTime + 1)
                {
                    previousTime = currentTime;
                    Vector3 rotationVector = _pointerTransform.localRotation.eulerAngles + new Vector3(0, 0, degreesToAdd);
                    _pointerTransform.localRotation = Quaternion.Euler(rotationVector);
                    _fillBar.fillAmount -= fillAmountToSubtract;
                }
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime = 0;
                previousTime = 0;
                Vector3 rotationVector = _pointerTransform.localRotation.eulerAngles + new Vector3(0, 0, degreesToAdd);
                _pointerTransform.localRotation = Quaternion.Euler(rotationVector);
                _fillBar.fillAmount -= fillAmountToSubtract;
                roundNumber++;
                playerStats.DecideRoundWinner();
                _roundTally.sprite = _tallySprites[roundNumber];
                _endOfRoundPanel.SetActive(true);
                playerStats.ShowStats();
                pauzeBeh.enabled = false;

                GameManager.GameStage = GameStages.RoundEnd;
                startRound = false;

                if (roundNumber < 5) StartCoroutine(WaitTimeBetweenRounds());
                else StartCoroutine(WaitTillStartMenu());
            }
        }        
    }

    private void Resets()
    {
        playerStats.ResetStats();

        for (int i = 0; i < allCookwheres.Length; i++)
        {
            CookwhereBehaviour beh = allCookwheres[i].GetComponent<CookwhereBehaviour>();
            beh.ResetCookwhere();
        }
    }

    IEnumerator WaitTimeBetweenRounds()
    {
        _counterTextObj.SetActive(true);
    
        for (int i = _waitUntilNewRound; i > 0; i--)
        {
            FindObjectOfType<AudioManager>().Play("CountDownSound");
            _counterText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
 
        StartRound();
    }

    IEnumerator WaitTillStartMenu()
    {
        _counterTextObj.SetActive(true);
        for (int i = _waitUntilNewRound; i > 0; i--)
        {
            _counterText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }

        playerStats.GameWinner();
        playerStats.ShowEndOfGameStats();

        for (int i = _waitUntilStartScreen; i > 0; i--)
        {
            _counterText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }

        Fireworks.SetActive(true);
        _bottomBorder.SetActive(false);
        _endOfRoundPanel.SetActive(false);
        winnerSprite.sprite = playerStats.ShowWinner();
        _winnerPanel.SetActive(true);

        yield return new WaitForSeconds(_waitUntilStartScreen);
        SceneManager.LoadScene("MainMenu");
    }
}