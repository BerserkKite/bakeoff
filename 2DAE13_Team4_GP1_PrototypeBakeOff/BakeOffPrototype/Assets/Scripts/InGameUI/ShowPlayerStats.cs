﻿using UnityEngine;
using UnityEngine.UI;

public class ShowPlayerStats : MonoBehaviour
{
    [SerializeField] Image[] playerTallies;
    [SerializeField] string[] _playerTag;
    [SerializeField] Sprite[] _tallySprites;
    [SerializeField] Image[] _playerIcons;

    PlayerBehaviour[] playerBehaviours;
    float _damageDone, _damageTaken;

    public void ShowStats()
    {
        for (int i = 0; i < _playerTag.Length; i++)
        {
            if (GameObject.FindGameObjectWithTag(_playerTag[i]) != null)
            {
                PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();

                playerTallies[i].sprite = _tallySprites[playerbeh.RoundsWon];
                _playerIcons[i].sprite = playerbeh.PlayerSprite;
            }
        }
    }
}
