﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauzeBehaviour : MonoBehaviour
{
    [SerializeField] int _playerControllerNumber;
    [SerializeField] StandaloneInputModule _inputModule;
    [SerializeField] GameObject _pauzeMenu;

    private void Start()
    {
        _pauzeMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameStage == GameStages.Pauze && Input.GetButtonDown("J" + _playerControllerNumber + "StartButton"))
        {
            GameManager.GameStage = GameStages.RoundPlaying;
            _pauzeMenu.SetActive(false);
        }

        if (CheckPauzeInput(1) && GameManager.GameStage != GameStages.Pauze) { _playerControllerNumber = 1; OpenPauzeMenu(); }
        else if (CheckPauzeInput(2) && GameManager.GameStage != GameStages.Pauze) { _playerControllerNumber = 2; OpenPauzeMenu(); }
        else if (CheckPauzeInput(3) && GameManager.GameStage != GameStages.Pauze) { _playerControllerNumber = 3; OpenPauzeMenu(); }
        else if (CheckPauzeInput(4) && GameManager.GameStage != GameStages.Pauze) { _playerControllerNumber = 4; OpenPauzeMenu(); }
    }

    private void OpenPauzeMenu()
    {
        GameManager.GameStage = GameStages.Pauze;
        _pauzeMenu.SetActive(true);

        _inputModule.horizontalAxis = "J" + _playerControllerNumber + "HorizontalAxis";
        _inputModule.verticalAxis = "J" + _playerControllerNumber + "VerticalAxis";
        _inputModule.submitButton = "J" + _playerControllerNumber + "AButton";
        _inputModule.cancelButton = "J" + _playerControllerNumber + "BButton";
    }

    private bool CheckPauzeInput(int v)
    {
        return Input.GetButtonDown("J" + v + "StartButton");
    }

    public void Resume()
    {
        GameManager.GameStage = GameStages.RoundPlaying;
        _pauzeMenu.SetActive(false);
    }

    public void QuitMatch()
    {
        UIMainMenu.Level = "MainMenu";
        SceneManager.LoadScene("Loading Screen");
    }
}
