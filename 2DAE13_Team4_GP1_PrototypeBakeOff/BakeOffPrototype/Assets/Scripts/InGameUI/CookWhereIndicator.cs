﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookWhereIndicator : MonoBehaviour
{
    List<Image> _indicators = new List<Image>();
    [SerializeField] GameObject _instantiationGrid;

    public void CreateIndicator(Image imageToInstantiate)
    {
        _indicators.Add(Instantiate(imageToInstantiate, _instantiationGrid.transform));
    }

    public void ClearIndicators()
    {
        for (int i = 0; i < _instantiationGrid.transform.childCount; i++)
        {
            Destroy(_instantiationGrid.transform.GetChild(i).gameObject);
        }

        _indicators.Clear();
    }
}
