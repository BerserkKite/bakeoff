﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GetPlayerStats : MonoBehaviour
{
    [SerializeField] string[] _playerTag;
    [SerializeField] Sprite[] _tallySprites;
    [SerializeField] Image[] _playerTally;
    [SerializeField] Image[] _playerIcons;
    [SerializeField] TextMeshProUGUI[] _damageDoneText, _damageTakenText;
    float _damageDone, _damageTaken;
    float[] _playerScores;
    float[] _playerDamageDone, _playerDamageTaken;

    SpawnPlayers spawnPlayers;

    PlayerBehaviour playerWon;

    public void Initialize()
    {
        _playerDamageDone = new float[_playerTag.Length];
        _playerDamageTaken = new float[_playerTag.Length];
    }

    public void ShowStats()
    {
        float AllDamageDone = 0;
        float AllDamageTaken = 0;

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();
            AllDamageDone += playerbeh.DamageDone;
            AllDamageTaken += playerbeh.DamageTaken;
            _playerDamageDone[i] += playerbeh.DamageDone;
            _playerDamageTaken[i] += playerbeh.DamageTaken;
        }

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();
            _damageDone = playerbeh.DamageDone;
            _damageTaken = playerbeh.DamageTaken;

            _damageDoneText[i].text = string.Format("{0:0.00} ", (_damageDone / AllDamageDone) * 100) + "%";
            _damageTakenText[i].text = string.Format("{0:0.00} ", (_damageTaken / AllDamageTaken) * 100) + "%";

            if (_damageDone == 0) _damageDoneText[i].text = string.Format("{0:0.00} ", 0) + "%";
            if (_damageTaken == 0) _damageTakenText[i].text = string.Format("{0:0.00} ", 0) + "%";

            _playerTally[i].sprite = _tallySprites[playerbeh.RoundsWon];
            _playerIcons[i].sprite = playerbeh.PlayerSprite;
        }
    }

    public void ShowEndOfGameStats()
    {
        float AllDamageDone = 0;
        float AllDamageTaken = 0;

        for (int i = 0; i < _playerTag.Length; i++)
        {
            AllDamageDone += _playerDamageDone[i];
            AllDamageTaken += _playerDamageTaken[i];
        }

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();

            _damageDoneText[i].text = string.Format("{0:0.00} ", (_playerDamageDone[i] / AllDamageDone) * 100) + "%";
            _damageTakenText[i].text = string.Format("{0:0.00} ", (_playerDamageTaken[i] / AllDamageTaken) * 100) + "%";

            if(_playerDamageDone[i] == 0) _damageDoneText[i].text = string.Format("{0:0.00} ", 0) + "%";
            if(_playerDamageTaken[i] == 0) _damageTakenText[i].text = string.Format("{0:0.00} ", 0) + "%";
            _playerTally[i].sprite = _tallySprites[playerbeh.RoundsWon];
        }
    }

    public Sprite ShowWinner()
    {
        return playerWon.PlayerSprite;
    }

    public void ResetStats()
    {
        spawnPlayers = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SpawnPlayers>();
        RespawnPlayers();

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();
            playerbeh.ResetPlayer();
        }
    }

    public void DecideRoundWinner()
    {
        PlayerBehaviour playerBehWinner = new PlayerBehaviour();
        float[] playerScores = new float[_playerTag.Length];        

        float highestScore = 0, highestDamageDoneScore = 0, LessDamageTaken = 0;

        for (int i = 0; i < _playerTag.Length; i++)
        {
            if (GameObject.FindGameObjectWithTag(_playerTag[i]) != null)
            {
                PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();
                playerScores[i] = playerbeh.DamageDone - playerbeh.DamageTaken;
            }          
        }

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();

            if (playerScores[i] < 0) playerScores[i] = 0;

            if (highestScore < playerScores[i])
            {
                highestScore = playerScores[i];
                playerBehWinner = playerbeh;
                highestDamageDoneScore = playerbeh.DamageDone;
                LessDamageTaken = playerbeh.DamageTaken;
            }
            else if (highestScore == playerScores[i])
            {
                if (highestDamageDoneScore < playerbeh.DamageDone)
                {
                    playerBehWinner = playerbeh;
                }
                else if (LessDamageTaken > playerScores[i])
                {
                    playerBehWinner = playerbeh;
                }
            }
        }

        _playerScores = playerScores;
        playerBehWinner.RoundsWon++;
    }

    public void GameWinner()
    {
        PlayerBehaviour[] playerResults = new PlayerBehaviour[_playerTag.Length];
        int[] roundsWon = new int[_playerTag.Length];

        for (int i = 0; i < _playerTag.Length; i++)
        {
            PlayerBehaviour playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i]).GetComponent<PlayerBehaviour>();
            roundsWon[i] = playerbeh.RoundsWon;
            playerResults[i] = playerbeh;
        }

        playerWon = new PlayerBehaviour();
        int maxRoundsWon = 0;
        float HighestScore = 0;
        float HighestDamageDone = 0;
        float HighestDamageTaken = 0;

        for (int i = 0; i < _playerTag.Length; i++)
        {
            if(roundsWon[i] > maxRoundsWon)
            {
                playerWon = playerResults[i];
                maxRoundsWon = roundsWon[i];
                HighestScore = _playerScores[i];
                HighestDamageDone = _playerDamageDone[i];
                HighestDamageTaken = _playerDamageTaken[i];
            }
            else if(roundsWon[i] == maxRoundsWon)
            {
                if(_playerScores[i] > HighestScore)
                {
                    playerWon = playerResults[i];
                    maxRoundsWon = roundsWon[i];
                    HighestScore = _playerScores[i];
                    HighestDamageDone = _playerDamageDone[i];
                    HighestDamageTaken = _playerDamageTaken[i];
                }
                else if(_playerDamageDone[i] > HighestDamageDone)
                {
                    playerWon = playerResults[i];
                    maxRoundsWon = roundsWon[i];
                    HighestScore = _playerScores[i];
                    HighestDamageDone = _playerDamageDone[i];
                    HighestDamageTaken = _playerDamageTaken[i];
                }
                else if(_playerDamageTaken[i] < HighestDamageTaken)
                {
                    playerWon = playerResults[i];
                    maxRoundsWon = roundsWon[i];
                    HighestScore = _playerScores[i];
                    HighestDamageDone = _playerDamageDone[i];
                    HighestDamageTaken = _playerDamageTaken[i];
                }
            }
        }
    }

    public void RespawnPlayers()
    {
        PlayerBehaviour playerbeh = null;
        spawnPlayers.Shuffle();
        for (int i = 0; i < _playerTag.Length; i++)
        {
            playerbeh = GameObject.FindGameObjectWithTag(_playerTag[i])?.GetComponent<PlayerBehaviour>();

            if (playerbeh == null) break;
            else
                playerbeh.Respawn(spawnPlayers.GetSpawnPoint(i), false);
        }
        if(playerbeh == null)
            spawnPlayers.InitializePlayers();
    }
}
