﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))] [RequireComponent(typeof(MeshCollider))]
public class ProductBehaviour : MonoBehaviour
{
    public Eatables EatableType;
    GameObject[] playerObjects = new GameObject[4];
    Rigidbody _rigid;
    float _damage, _forceAmount;
    public string PlayerTag;

    
    public bool IsThrowen = false;

    public bool IsBomb = false;
    public float _explodeRadius = 5f;

    PlayerBehaviour playerBehaviour;

    public MeshFilter MeshFilter;
    public Renderer Renderer;
    MeshCollider _collider;

    FoodEngine _engine;

    bool _checkForCollisions;

    private bool _isActive = true;

    private bool _canThrow = false;
    private float _timer;

    [SerializeField] GameObject _damageNumberPrefab;
    [SerializeField] Material _highLightMaterial;
    [SerializeField] GameObject _pickUpIndicator;
    [SerializeField] Text _text;

    private void Start()
    {
        _pickUpIndicator.SetActive(false);

        _text = GetComponent<Text>();
        _rigid = GetComponent<Rigidbody>();

        _engine = new FoodEngine(_rigid, playerBehaviour, transform);

        _collider = GetComponent<MeshCollider>();
        MeshFilter = GetComponentInChildren<MeshFilter>();
        Renderer = GetComponentInChildren<Renderer>();
        ChangeMesh();

        if (EatableType.paintDiameter > 0)
        {
            _isActive = true;
        }
        CheckForPlayers();
    }

    private void CheckForPlayers()
    {
        for (int i = 0; i < playerObjects.Length; i++)
        {
            playerObjects[i] = GameObject.FindGameObjectWithTag("Player" + (i + 1));
        }
    }

    private void ChangeMesh()
    {
        _collider.sharedMesh = EatableType.VisualObjectMesh;
        MeshFilter.sharedMesh = EatableType.VisualObjectMesh;
        Renderer.material = EatableType.VisualMaterial;
    }

    public void ChangeToHighlightMaterial(GameObject go)
    {
        Renderer.sharedMaterial = _highLightMaterial;
        if (go == null && PlayerPrefs.GetInt("Indicator") == 1) _pickUpIndicator.SetActive(true);
    }

    public void ChangeToNormalMaterial()
    {
        Renderer.sharedMaterial = EatableType.VisualMaterial;
        _pickUpIndicator.SetActive(false);
    }

    private void Update()
    {
        if (transform.parent != null)
        {
            _rigid.isKinematic = true;
            CheckForPlayers();
        }
    }

    public void ThrowProduct(Vector3 direction, Eatables productType, string playerTag, Quaternion rotation)
    {
        _engine.PlayerBehaviour = GetComponentInParent<PlayerBehaviour>();
        _engine.Transform = transform;
        transform.rotation = rotation;
        CheckThrowMechanic(direction, productType, playerTag);
        StartChecking();
        PlayerTag = playerTag;
        IsThrowen = true;
    }

    private void CheckThrowMechanic(Vector3 direction, Eatables productType, string playerTag)
    {
        switch (productType.ThrowMechanic)
        {
            case ThrowMechanicNames.NormalThrow:
                _engine.NormalThrow(direction, productType);
                break;
            case ThrowMechanicNames.NinjaThrow:
                _engine.NinjaThrow(direction, productType, playerTag);
                break;
            case ThrowMechanicNames.RapidThrow:
                _engine.RapidThrow(direction, productType, _canThrow);
                StartCoroutine(WaitForThrow(0.3f, direction, productType));
                break;
            case ThrowMechanicNames.ExplodigThrow:
                _engine.ExplodingThrow(direction, productType);
                break;
            case ThrowMechanicNames.Melee:
                _rigid.isKinematic = true;
                transform.rotation = new Quaternion(transform.rotation.x + 0.5f, transform.rotation.y, transform.rotation.z, transform.rotation.w);
                break;
        }
    }

    IEnumerator WaitForThrow(float delay, Vector3 direction, Eatables productType)
    {
        yield return new WaitForSeconds(delay);
        _engine.RapidThrow(direction, productType, _canThrow);
        FindObjectOfType<AudioManager>().Play("ThrowSound");
        yield return new WaitForSeconds(delay);
        FindObjectOfType<AudioManager>().Play("ThrowSound");
        _canThrow = true;
        _engine.RapidThrow(direction, productType, _canThrow);
    }

    public void StartChecking()
    {
        StartCoroutine(StartChecks());
    }

    IEnumerator StartChecks()
    {
        _collider = GetComponent<MeshCollider>();
        _collider.enabled = false;
        yield return new WaitForSeconds(0.1f);
        _checkForCollisions = true;
        _collider.enabled = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == PlayerTag) return;

        if (collision.collider.TryGetComponent(out ProductBehaviour pb))
        {
            bool returnMethod = false;
            if (pb.PlayerTag == PlayerTag) returnMethod = true;
            if (returnMethod) return;
        }

        if (_checkForCollisions)
        {
            PlayerBehaviour playerBeh = GameObject.FindGameObjectWithTag(PlayerTag)?.GetComponent<PlayerBehaviour>();
            if (playerBeh != null)
            {
                foreach (var player in playerObjects)
                {
                    if (collision.collider.CompareTag(player.tag))
                    {
                        DamageNumbersBehavior.Create(player.transform,EatableType.DamageAmount, _damageNumberPrefab.transform);
                        playerBeh.DamageDone += EatableType.DamageAmount;
                    }
                }
            }
            if (IsBomb)
            {
                _timer += Time.deltaTime;
                if (_timer > 3)
                {
                    BombExplode();
                    _timer = 0;
                    return;
                }
            }

            if (transform.parent == null)
                Destroy(this.gameObject);
            else _checkForCollisions = false;
        }

        if (!_checkForCollisions)
            return;
        
        if (!_isActive)
            return;

        PaintProjectileManager manager = GameManager.GetInstance().GetProjectileManager();
        for (int i = 0; i < 5; ++i)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, manager.GetRandomSphereRay(), out hit, EatableType.paintDiameter))
            {

                if (hit.collider is MeshCollider || hit.collider is BoxCollider)
                {
                    MyShaderBehavior script = hit.collider.gameObject.GetComponent<MyShaderBehavior>();
                    if (null != script)
                    {
                        script.PaintOnColored(hit.textureCoord, manager.GetProjectileSplash(EatableType.Name), EatableType.paintColor);
                    }
                }
            }
        }
    }

    private void BombExplode()
    {
        Debug.Log("BOOM!");
        GameObject[] players = new GameObject[4];
        for (int i = 1; i < 5; i++)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player" + i);
            players[i - 1] = player;
        }

        foreach (var player in players)
        {
            var distance = Vector3.Distance(transform.position, player.transform.position);
            var direction = Vector3.Normalize((transform.position - player.transform.position));
            if (distance < _explodeRadius)
            {
                player.GetComponent<PlayerBehaviour>().DamageDone += EatableType.DamageAmount;
                player.GetComponent<Rigidbody>().AddForce(direction * EatableType.ForceAmount, ForceMode.Impulse);
            }
        }
        Destroy(gameObject);
    }

    public void Release()
    {
        transform.parent = null;
        _rigid.isKinematic = false;
    }

    public void Destroy()
    {
        Destroy(this);
    }
}