﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPlayers : MonoBehaviour
{
    public List<GameObject> _players;
    GameObject[] _spawnPoints;
    List<GameObject> _spawnPointList;
    PlayerBehaviour _respawnPlayer;

    private void Awake()
    {
        _players.Clear();
        _players = GameManager.PlayerPrefabs;
        GetSpawnPoints();
    }

    public void Shuffle()
    {
        for (int i = 0; i < _spawnPointList.Count; i++)
        {
            int rnd = Random.Range(0, i);
            GameObject temp = _spawnPointList[i];

            _spawnPointList[i] = _spawnPointList[rnd];
            _spawnPointList[rnd] = temp;
        }
    }

    public Transform GetSpawnPoint(int i)
    {
        return _spawnPointList[i].transform;
    }

    public void InitializePlayers()
    {
        Shuffle();
        for (int i = 0; i < _players.Count; i++)
        {
            GameObject spawnedPlayer = Instantiate(_players[i], _spawnPointList[i].transform);
            spawnedPlayer.GetComponent<PlayerBehaviour>().InitializePosition = _spawnPointList[i].transform;
            spawnedPlayer.GetComponent<PlayerBehaviour>().Playernumber = i + 1;
        }
    }

    private void GetSpawnPoints()
    {
        _spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        _spawnPointList = _spawnPoints.ToList();
    }

    public void GetFurthestSpawnPointFromPlayers(PlayerBehaviour playerBeh)
    {
        _respawnPlayer = playerBeh;
        GameObject getFurthest = GetFurthestSpawnPointFromPlayers();

        if (getFurthest == null) return;
        else
        {
            playerBeh.Respawn(getFurthest.transform, true);
        }
    }

    private GameObject GetFurthestSpawnPointFromPlayers()
    {
        GameObject furthestGO = null;
        float distance = 0f;

        for (int i = 0; i < _spawnPointList.Count; i++)
        {
            if (GetFurthestDistance(_spawnPointList[i]) > distance)
            {
                furthestGO = _spawnPointList[i];
                distance = GetFurthestDistance(_spawnPointList[i]);
            }
            //Debug.Log($"Spawnpoint {i}, position {_spawnPointList[i].transform.position}, distance {GetFurthestDistance(_spawnPointList[i])}");
        }
        return furthestGO;
    }

    private float GetFurthestDistance(GameObject spawnPoint)
    {
        //Debug.Log($"Player1 distance: {CheckDistance(spawnPoint, 0)}, \n Player2 distance: {CheckDistance(spawnPoint, 1)}, \n Player3 distance: {CheckDistance(spawnPoint, 2)}, \n Player4 distance: {CheckDistance(spawnPoint, 3)}");
        return (CheckDistance(spawnPoint, 0) + CheckDistance(spawnPoint, 1) + CheckDistance(spawnPoint, 2) + CheckDistance(spawnPoint, 3)) / _spawnPointList.Count;
    }

    private float CheckDistance(GameObject spawnPoint, int i)
    {
        if (i+1 != _respawnPlayer.Playernumber)
        {
            return (GetPlayer(i).transform.position - spawnPoint.transform.position).magnitude;
        }
        else return 0f;
    }

    private GameObject GetPlayer(int i)
    {
        return GameObject.FindGameObjectWithTag("Player" + (i + 1));
    }
}