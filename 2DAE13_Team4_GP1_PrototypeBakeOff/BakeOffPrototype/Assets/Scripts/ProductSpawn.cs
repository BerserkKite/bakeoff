﻿using System.Collections;
using UnityEngine;

public class ProductSpawn : MonoBehaviour
{
    [SerializeField] Eatables[] _eatablesToSpawn;
    [SerializeField] GameObject _placeHolder;
    [SerializeField] float _waitTillnextSpawn = 1f;
    int _randomValue; bool startSpawn;

    private void Start()
    {
        GenerateRandomValue();
    }

    private void GenerateRandomValue()
    {
        _randomValue = Random.Range(0, _eatablesToSpawn.Length);
    }

    private void Update()
    {
        if (this.transform.childCount == 0 && !startSpawn) { StartCoroutine(SpawnNewProduct()); startSpawn = true; }
    }

    IEnumerator SpawnNewProduct()
    {
        yield return new WaitForSeconds(_waitTillnextSpawn);
        _placeHolder.GetComponent<ProductBehaviour>().EatableType = _eatablesToSpawn[_randomValue];
        GameObject go = Instantiate(_placeHolder, this.transform);
        go.transform.parent = this.transform;
        GenerateRandomValue();
        startSpawn = false;
    }
}
