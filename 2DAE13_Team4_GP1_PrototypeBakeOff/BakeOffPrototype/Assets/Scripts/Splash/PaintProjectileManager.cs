﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintProjectileManager : MonoBehaviour
{
    public Texture2D[] projectileSplashTextures;


    private List<float[,]> _splashTextureList;
    private int _totalSplashTextures;

    void Start()
    {
        _totalSplashTextures = projectileSplashTextures.Length;
        _splashTextureList = new List<float[,]>(_totalSplashTextures);
        for (int i = 0; i < _totalSplashTextures; ++i)
        {
            Texture2D texture = projectileSplashTextures[i];
            int textureWidth = texture.width;
            int textureHeight = texture.height;
            Color[] currTexture = texture.GetPixels();
            float[,] textureAlphas = new float[textureWidth, textureHeight];
            int counter = 0;
            for (int x = 0; x < textureWidth; ++x)
            {
                for (int y = 0; y < textureHeight; ++y)
                {
                    textureAlphas[x, y] = currTexture[counter].a;
                    counter++;
                }
            }
            _splashTextureList.Add(textureAlphas);
        }
    }

    public float[,] GetProjectileSplash(ProductNames Name)
    {       
        if (Name == ProductNames.Tomato)
            return _splashTextureList[0];
        if (Name == ProductNames.Chicken)
            return _splashTextureList[1];
        if (Name == ProductNames.Cabbage)
            return _splashTextureList[2];
        if (Name == ProductNames.Steak)
            return _splashTextureList[3];
        if (Name == ProductNames.SteakSalad)
            return _splashTextureList[4];
        if (Name == ProductNames.TomatoSlices)
            return _splashTextureList[5];
        if (Name == ProductNames.TomatoSalad)
            return _splashTextureList[6];
        return null;
    }
    
    public Vector3 GetRandomSphereRay()
    {
        return Random.onUnitSphere;
    }
}
