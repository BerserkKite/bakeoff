﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class Configuration
{
    public float Speed;
    public float FallSpeed;
    public float Gravity = 1;
    public float SpringtrapEffect = 2.5f;
}

public class PlayerEngine 
{
    private readonly CharacterController _controller;

    private Transform _transform;
    private Vector3 _velocity;
    private Configuration _configuration;
    private Animator _animtor;

    public PlayerEngine(CharacterController controller, Transform transform, Configuration engineConfiguration, Animator animator)
    {
        _controller = controller;
        _configuration = engineConfiguration;
        _transform = transform;
        _animtor = animator;       
    }

    public void Begin()
    {
        _velocity = _controller.velocity;
        Gravity();
    }

    private void Gravity()
    {
        _velocity += Physics.gravity * _configuration.Gravity * Time.deltaTime * _configuration.FallSpeed;
    }

    public void Ground(Vector3 Normal)
    {
        _velocity = Vector3.Scale(_velocity, new Vector3(0, 1, 0));
        _velocity = Vector3.ProjectOnPlane(_velocity, Normal);
    }

    public void Movement(Vector3 normal, Vector3 movingDirection)
    {
        Vector3 ground = movingDirection;
        Vector3.OrthoNormalize(ref normal, ref ground);
        _velocity += ground * movingDirection.magnitude * _configuration.Speed;
        if (movingDirection.magnitude != 0)
            _animtor.SetBool("IsIdle", false);
        else
            _animtor.SetBool("IsIdle", true);
    }

    public void ApplySpringTrap()
    {
        Vector3 movingDirection = _transform.forward;
        movingDirection = Vector3.Lerp(movingDirection, new Vector3( _transform.forward.x , _transform.forward.y * _configuration.SpringtrapEffect, _transform.forward.z), Time.deltaTime);

        _velocity += movingDirection.normalized * _configuration.SpringtrapEffect;
    }

    public void Impact(Vector3 impact)
    {
        _controller.Move(impact * Time.deltaTime);
    }

    public void Respawn()
    {
        _velocity = Vector3.zero;
    }

    public void Commit()
    {
        _controller.Move(_velocity * Time.deltaTime);
    }     
}
public class FoodEngine 
{
    Rigidbody _rigid;
    PlayerBehaviour _playerBehaviour;
    Transform _transform;

    List<Quaternion> _extraObj = new List<Quaternion>();
    List<Rigidbody> _extraRigids = new List<Rigidbody>();
    List<GameObject> _extraTomatoes = new List<GameObject>();
    
    private float _angle = 5f;
    private float _timer;
    private bool _nextThrow = true;

    public FoodEngine(Rigidbody rigid, PlayerBehaviour playerBehaviour, Transform transform)
    {
        _rigid = rigid;
        PlayerBehaviour = playerBehaviour;
        Transform = transform;
    }

    public PlayerBehaviour PlayerBehaviour { get => _playerBehaviour; set => _playerBehaviour = value; }
    public Transform Transform { get => _transform; set => _transform = value; }

    public void NormalThrow(Vector3 direction, Eatables type)
    {
        _rigid.isKinematic = false;
        _rigid.AddForce(direction * type.ForceAmount, ForceMode.Impulse);
        _rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);
        PlayerBehaviour.HasObject = false;
        Transform.parent = null;

    }

    public void NinjaThrow(Vector3 direction, Eatables type, string playerTag)
    {
        _rigid.isKinematic = false;
        _rigid.transform.eulerAngles = new Vector3(0,_rigid.transform.parent.eulerAngles.y,0);
        _extraRigids.Add(_rigid);

        for (int i = -1; i < 2; i+=2)
        {
            Quaternion extraObj = Quaternion.Euler(_rigid.rotation.eulerAngles);

            _extraObj.Add(extraObj);
        }

        int j=-1;
        foreach (Quaternion obj in _extraObj)
        {
            GameObject go = GameObject.Instantiate(_rigid.gameObject, _rigid.transform);
            ProductBehaviour pb = go.GetComponent<ProductBehaviour>();
            pb.PlayerTag = playerTag;
            pb?.StartChecking();
            go.transform.localScale = Vector3.one;
            go.transform.forward = direction;
            Rigidbody rigid = go.GetComponent<Rigidbody>();
            rigid.isKinematic = false;
            go.transform.Rotate(go.transform.up,j*_angle);
            go.transform.parent = null;

            rigid.AddForce(go.transform.forward * type.ForceAmount, ForceMode.Impulse);
            rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);
            j +=2;
            //obj.AddForce(obj.transform.parent.right*(1- (_extraObj.IndexOf(obj)*2)) * product.ForceReturn, ForceMode.Impulse);
        }
        _rigid.AddForce(direction * type.ForceAmount, ForceMode.Impulse);
        _rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);

        PlayerBehaviour.HasObject = false;

        Transform.parent = null;
    }

    public void RapidThrow(Vector3 direction, Eatables type, bool canThrow)
    {
        _rigid.isKinematic = false;
        _rigid.transform.eulerAngles = new Vector3(0, _rigid.transform.parent.eulerAngles.y, 0);
        if (!canThrow)
        {
            ThrowObject(direction, type);
        }
        if (canThrow)
        {
            _rigid.AddForce(direction * type.ForceAmount, ForceMode.Impulse);
            _rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);
            PlayerBehaviour.HasObject = false;
            Transform.parent = null;
        }
    }

    private void ThrowObject(Vector3 direction, Eatables type)
    {
        GameObject extraObject =
            GameObject.Instantiate(_rigid.gameObject, _rigid.transform.position, _rigid.transform.rotation);
        ProductBehaviour pb = extraObject.GetComponent<ProductBehaviour>();
        pb?.StartChecking();
        extraObject.transform.localScale = Vector3.one;
        extraObject.transform.forward = direction;
        Rigidbody rigid = extraObject.GetComponent<Rigidbody>();
        rigid.AddForce(extraObject.transform.forward * type.ForceAmount, ForceMode.Impulse);
        rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);
    }

    public void ExplodingThrow(Vector3 direction, Eatables type)
    {
        _rigid.isKinematic = false;
        _rigid.AddForce(direction * type.ForceAmount, ForceMode.Impulse);
        _rigid.AddForce(Vector3.up * type.ForceAmount / 2, ForceMode.Impulse);
        ProductBehaviour pb = _rigid.GetComponent<ProductBehaviour>();
        pb.IsBomb = true;
        PlayerBehaviour.HasObject = false;
        Transform.parent = null;

    }
}