﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButtonSound : MonoBehaviour, ISelectHandler
{
    public void PlayClickSound()
    {
        FindObjectOfType<AudioManager>().Play("ClickSound");
    }
    public void PlayHoverSound()
    {
        FindObjectOfType<AudioManager>().Play("HoverSound");
    }
    public void PlayPageTurnSound()
    {
        FindObjectOfType<AudioManager>().Play("PageFlipSound");
    }

    public void OnSelect(BaseEventData eventData)
    {
        FindObjectOfType<AudioManager>().Play("HoverSound");
    }
}
