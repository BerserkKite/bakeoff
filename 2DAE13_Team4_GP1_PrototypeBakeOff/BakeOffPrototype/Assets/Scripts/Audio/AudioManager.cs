﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sounds[] Sounds;
    public void Awake()
    {
        foreach (Sounds s in Sounds)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.Clip;
            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;
            s.Source.loop = s.Loop;
        }
    }
    private void Start()
    {
       //theme sound
    }
    public void Play(string name)
    {
       Sounds s = Array.Find(Sounds, sound => sound.Name == name);     
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
           
        s.Source.Play();
    }
    public void Stop(string name)
    {
        Sounds s = Array.Find(Sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }

        s.Source.Stop();
    }
   
}
