﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{
    [SerializeField] GameObject _damageNumberPrefab;
    [SerializeField] private Transform[] _waypoints;
    [SerializeField] private int _index = 0;
    [SerializeField] private float _maxDistance = 0;
    [SerializeField] private Animator _animator;
    [Range(0, 2)] [SerializeField] private float _speed;
    [SerializeField] float _sawDamage = 2f;

    bool _canDoDamage = true;

    void Update()
    {
        MoveToWaypoint();
    }

    void MoveToWaypoint()
    {

        Vector3 direction = _waypoints[_index].transform.position - transform.position;

        if (direction.magnitude <_maxDistance)
        if (_index < _waypoints.Length - 1)
            {
                ++_index;
                _animator.SetBool("IsMirror", false);
            }        
        else
            {
                _animator.SetBool("IsMirror", true);
                _index = 0;
            }
            
       transform.position = Vector3.MoveTowards(transform.position, _waypoints[_index].position, _speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < 4; i++)
        {
            if (other.CompareTag("Player" +(i+1)) && _canDoDamage)
            {
                _canDoDamage = false;
                other.GetComponent<PlayerBehaviour>().DamageTaken += _sawDamage;
                StartCoroutine(WaitTillNextDamageTick());
                DamageNumbersBehavior.Create(other.transform, _sawDamage, _damageNumberPrefab.transform);
            }                
        }
    }

    IEnumerator WaitTillNextDamageTick()
    {
        yield return new WaitForSeconds(5f);
        _canDoDamage = true;
    }
}
