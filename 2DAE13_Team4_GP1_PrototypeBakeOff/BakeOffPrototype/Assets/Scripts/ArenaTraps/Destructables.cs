﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructables: MonoBehaviour
{
    [SerializeField] private Mesh _MeshFase1;
    [SerializeField] private Mesh _MeshFase2;
    [SerializeField] private Mesh _MeshFase3;
    [SerializeField] private MeshFilter _MainMesh;

    [SerializeField] private float _health;
    private float _healthV;

    private void Start()
    {
        _healthV = _health;
    }
    void Update()
    {
        if(_health <= _healthV / 3 * 1)
        {
            _MainMesh.sharedMesh = _MeshFase3;
            this.GetComponent<MeshCollider>().sharedMesh = _MeshFase3;
        }
        else if (_health <= _healthV / 3 * 2)
        {
            _MainMesh.sharedMesh = _MeshFase2;
            this.GetComponent<MeshCollider>().sharedMesh = _MeshFase2;
        }
        else
        {
            _MainMesh.sharedMesh = _MeshFase1;
            this.GetComponent<MeshCollider>().sharedMesh = _MeshFase1;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Product"))
        {
            ProductBehaviour pb = collision.collider?.GetComponent<ProductBehaviour>();
            _health -= pb.EatableType.DamageAmount;
        }
    }
}
