﻿using System;
using System.Collections;
using UnityEngine;

public class Gass : MonoBehaviour
{
    [SerializeField] private float _gassDmg = 0.01f;
    [SerializeField] private GameObject _gas;
    public bool Active = false;
    public bool CanChange = true;
    
    void OnTriggerStay(Collider c)
    {
        for (int i = 0; i < 4; i++)
        {
            if (c.CompareTag("Player" + (i + 1))&& Active)
            {
                c.GetComponent<PlayerBehaviour>().DamageTaken += _gassDmg;
            }
        }
    }

    void ChangeMaterial()
    {
        if (Active)
        {
            _gas.SetActive(true);
        }
        else
        {
            _gas.SetActive(false);
        }
    }

    void SwitchState()
    {
        Active = !Active;
    }

    public IEnumerator StartTimer(int min, int max)
    {
        yield return new WaitForSeconds(GenerateRandom(min, max));
        if (Active)
        {
            SwitchState();
            ChangeMaterial();
        }
        else if(CanChange && !Active)
        {
            SwitchState();
            ChangeMaterial();
        }
        StartCoroutine(StartTimer(min, max));
    }

    int GenerateRandom(int min, int max)
    {
        return UnityEngine.Random.Range(min, max);
    }
}
