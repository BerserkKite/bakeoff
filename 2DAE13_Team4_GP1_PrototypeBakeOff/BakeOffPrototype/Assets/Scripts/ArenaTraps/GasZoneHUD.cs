﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasZoneHUD : MonoBehaviour
{
    [SerializeField] Gass[] _gasZones;
    [SerializeField] int _zonesNonActiveAtATime = 2;
    [Range(5, 10)][SerializeField] int minTime;
    [Range(15, 20)][SerializeField] int maxTime;

    void Start()
    {
        for (int i = 0; i < _gasZones.Length; i++)
        {
            _gasZones[i].StartCoroutine(_gasZones[i].StartTimer(minTime, maxTime));
        }
    }

    private void Update()
    {
        CheckGasZones();
    }

    private void CheckGasZones()
    {
        int activeAmount = 0;
        foreach (var zone in _gasZones)
        {
            if (zone.Active) activeAmount++;
        }
        foreach (var zone in _gasZones)
        {
            if (activeAmount >= _gasZones.Length - _zonesNonActiveAtATime) zone.CanChange = false;
            else zone.CanChange = true;
        }
    }
}
