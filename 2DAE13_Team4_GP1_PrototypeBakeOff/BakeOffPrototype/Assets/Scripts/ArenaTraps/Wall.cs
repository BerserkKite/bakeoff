﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{
    [SerializeField] private GameObject _wall;
    [SerializeField] private Vector2 _minAndMaxHold;
    private float _timerMove =0;
    private float _timerMoveValue = 2.1f;

    private bool _isGoingUp = true;
    private float _direction;

    private float _timerHold;

    void Update()
    {
        //timer gaat aftellen
        _timerMove += Time.deltaTime;        
        //zolang de timer nog aftelt
        if (_timerMove <= _timerMoveValue)
        {
            Move();
        }//zodra hij stopt
        else
        {          
            Hold();           
        }
            
    }

    private void Hold()
    {
        _timerHold += Time.deltaTime;
        if (_timerHold >= Random.Range(_minAndMaxHold.x, _minAndMaxHold.y))
        {
            changeDirection();
            _timerMove = 0;
            _timerHold = 0;
        }
    }

    private void Move()
    {
        if (_isGoingUp) { _direction = Time.deltaTime / 2; }
        else { _direction = -Time.deltaTime / 2; }

        _wall.transform.Translate(Vector3.up * _direction);
    }

    private void changeDirection()
    {
        if (_isGoingUp)
        {
            _isGoingUp = false;
        }
        else
        {
            _isGoingUp = true;
        }
      
    }
}
