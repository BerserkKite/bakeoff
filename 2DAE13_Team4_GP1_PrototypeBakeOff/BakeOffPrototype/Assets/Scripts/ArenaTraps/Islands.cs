﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Islands : MonoBehaviour
{
    [SerializeField] private Transform[] _waypoints;
    [SerializeField] private int _index = 0;
    [SerializeField] private float _maxDistance = 0;
    [Range(0, 2)] [SerializeField] private float _speed;

    void Update()
    {
        MoveToWaypoint();
    }

    void MoveToWaypoint()
    {

        Vector3 direction = _waypoints[_index].transform.position - transform.position;

        if (direction.magnitude < _maxDistance)
            if (_index < _waypoints.Length - 1)
            {
                ++_index;
            }
            else
            {
                _index = 0;
            }

        transform.position = Vector3.MoveTowards(transform.position, _waypoints[_index].position, _speed * Time.deltaTime);
    }
}
