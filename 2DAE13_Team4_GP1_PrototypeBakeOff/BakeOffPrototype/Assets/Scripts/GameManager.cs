﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_instance;
    private PaintProjectileManager m_projectileManager;
    public static List<GameObject> PlayerPrefabs;

    public static GameStages GameStage;

    private void Awake()
    {
        m_projectileManager = GetComponent<PaintProjectileManager>();

        if (null != m_instance)
            Destroy(gameObject);
        else
        {
            m_instance = this;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene("Lvl1");
    }
    public static GameManager GetInstance()
    {
        return m_instance;
    }
    public PaintProjectileManager GetProjectileManager()
    {
        return m_projectileManager;
    }
}
