﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    private string aButton, bButton;
    public PlayerSetup[] _players = new PlayerSetup[4];

    private List<int> _activePlayers = new List<int>();
    public static List<GameObject> PrefabPlayers = new List<GameObject>(4);

    private float _timer;
    bool startMatch = false;

    [SerializeField] GameObject button, countdown;
    [SerializeField] Text buttonText, countdownText;
    [SerializeField] int countDownValue = 5;

    string controller;
    public string[] joysticks;

    // Start is called before the first frame update
    void Start()
    {
        button.SetActive(false);
        _timer = countDownValue;
        joysticks = Input.GetJoystickNames();
        for (int i = 0; i < joysticks.Length; i++)
        {
            joysticks[i] = (i + 1).ToString();
        }
        Debug.Log(joysticks);
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        CheckStartGame();        
    }

    private void CheckSkin()
    {
        foreach(var player in _activePlayers)
        {
            _players[player - 1].CurrentPrefab.GetComponent<PlayerBehaviour>().Playernumber = player-1;
            PrefabPlayers.Add(_players[player-1].CurrentPrefab);
        }
    }

    public void ButtonAction()
    {
        startMatch = !startMatch;

        if (startMatch)
        buttonText.text = "Cancel";  
        else  buttonText.text = "Start";

        if (buttonText.text == "Cancel")
            FindObjectOfType<AudioManager>().Play("CountDownSound");
        if (buttonText.text == "Start") FindObjectOfType<AudioManager>().Stop("CountDownSound");

    }
   
    private void CheckStartGame()
    {
        if (_activePlayers.Count == 4)
        {
            button.SetActive(true);
            if (startMatch)
            {
                countdown.SetActive(true);
                countdownText.text = "" + ((int)(_timer + 0.5f));
                _timer -= Time.deltaTime;
                

            }
            else
            {
                _timer = countDownValue;
                countdown.SetActive(false);
                countdownText.text = countDownValue.ToString();
               
            }

            if (_timer < 0)
            {
                if (GameManager.PlayerPrefabs != null)
                {
                    GameManager.PlayerPrefabs.Clear();
                }
                CheckSkin();
                GameManager.PlayerPrefabs = PrefabPlayers;
                
                SceneManager.LoadScene("Loading Screen");
               
            }
        }
        else
        {
            button.SetActive(false);
            countdown.SetActive(false);
            countdownText.text = countDownValue.ToString();
            startMatch = false;
            buttonText.text = "Start";
            _timer = countDownValue;
            FindObjectOfType<AudioManager>().Stop("CountDownSound");
        }
    }
  
    void CheckInput()
    {
        //to join
        for (int i = 1; i < _players.Length + 1; i++)
        {
            if (!_activePlayers.Contains(i) && Input.GetButtonDown("J" + i + "AButton"))
            {

                FindObjectOfType<AudioManager>().Play("ClickSound");
                _activePlayers.Add(i);
                _players[GetPlayer()].Active(i);
            }
        }

        //to back out
        for (int i = 1; i < 5; i++)
        {
            if (_activePlayers.Contains(i) && Input.GetButtonDown("J" + i + "XButton"))
            {
                FindObjectOfType<AudioManager>().Play("ClickSound");
                _activePlayers.Remove(i);
                DeactivePlayer(i);
            }
        }
        if (Input.GetButtonDown("BButton"))
        {
            FindObjectOfType<AudioManager>().Play("ClickSound");
            SceneManager.LoadScene("MainMenu");
        }
    }

    int GetPlayer()
    {

        for(int i = 0;i<_players.Length;i++)
            if (!_players[i].enabled)
            {
                return i;
            }        
        return 0;
    }

    void DeactivePlayer(int ctrl)
    {
        for (int i = 0; i < _players.Length; i++)
        {
            if (_players[i].enabled && _players[i].controllerID.Equals(ctrl))
            {
                _players[i].DeActive();
                return;
            }
        }
    }
}
