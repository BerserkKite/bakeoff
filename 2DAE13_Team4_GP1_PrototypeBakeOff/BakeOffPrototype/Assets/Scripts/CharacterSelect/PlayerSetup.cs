﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSetup : MonoBehaviour
{
    public int playerID = 1;
    public int controllerID = 1;

    private GameObject _player;


    public GameObject[] _playerPrefabs;
    public GameObject CurrentPrefab;

    private int i;
    private void Start()
    {

    }
    void Update()
    {
        if (Input.GetButtonDown("J" + controllerID + "BumperRight"))
        {
            FindObjectOfType<AudioManager>().Play("HoverSound");
            i++;
            if (i > 3)
            {
                i = 0;
            }
            ChangePlayer(i);
        }
        if (Input.GetButtonDown("J" + controllerID + "BumperLeft"))
        {
            FindObjectOfType<AudioManager>().Play("HoverSound");
            i--;
            if (i < 0)
            {
                i = 3;
            }
            ChangePlayer(i);
        }
        CurrentPrefab = _playerPrefabs[i];
        /*else if (Input.GetButtonDown("Left_P" + controllerID))
        {
            iTex = iTex-1 < 0 ? texs.Length-1 : iTex-1;
            matDevil.SetTexture("_MainTex", texs[iTex]);
        }*/
    }

    private void ChangePlayer(int i)
    {
        Destroy(_player);
        _playerPrefabs[i].SetActive(true);
        _player = GameObject.Instantiate(_playerPrefabs[i], this.transform);
        _player.GetComponent<PlayerBehaviour>().enabled = false;    
        _player.transform.Rotate(Vector3.up, 180);
        _player.GetComponentInChildren<Animator>().SetBool("IsIdle", true);
        _player.GetComponentInChildren<Animator>().SetBool("IsDancing", true);
        StartCoroutine(WaitAfterDance(1f));
    }


    public void Active(int ctrl)
    {
        this.enabled = true;
        i = 0;
        _player = GameObject.Instantiate(_playerPrefabs[i],this.transform);
        _player.GetComponent<PlayerBehaviour>().enabled = false;
        _player.transform.Rotate(Vector3.up,180);
        controllerID = ctrl;
        _player.GetComponentInChildren<Animator>().SetBool("IsIdle", true);
        _player.GetComponentInChildren<Animator>().SetBool("IsDancing", true);
        StartCoroutine(WaitAfterDance(1f));

       

    }
    IEnumerator WaitAfterDance(float delay)
    {
        yield return new WaitForSeconds(delay);
        _player.GetComponentInChildren<Animator>().SetBool("IsDancing", false);
        _player.GetComponentInChildren<Animator>().SetBool("IsIdle", true);

    }
    public void DeActive()
    {
        this.enabled = false;
        _player.GetComponentInChildren<Animator>().SetBool("IsIdle", true);
        Destroy(_player);
      

    }
}
