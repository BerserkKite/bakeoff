﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Recipes", order = 2)]
public class Recipes : ScriptableObject
{
    public Eatables[] EatablesNeeded;
    public Eatables ReturnEatable;

    private List<Eatables> eatablesNeeded = new List<Eatables>();

    public bool IsMakeable = false;

    SortByName sortByName = new SortByName();

    public void Initialize()
    {
        eatablesNeeded = EatablesNeeded.ToList();
        IsMakeable = false;
    }

    public bool IsEatableInRecipe(Eatables eatable, List<Eatables> eatables) //check if the eatable is in this recipe
    {
        bool inRecipe = false;
        bool makeable = false;

        if (eatables.Count > 0)
        {
            eatablesNeeded.Sort(sortByName); //sort the needed list by name
            eatables.Sort(sortByName); //sort the input list by name

            foreach (var needed in eatablesNeeded)
            {
                foreach (var input in eatables)
                {
                    if (input.EatableName == needed.EatableName && eatable.EatableName != input.EatableName) { makeable = true;} //check if the eatable already in the list is the same one of the eatables needed in the recipe and if it isn't already in the recipe
                    else { makeable = false;  } //it doesn't exist in the recipe or it has already been inputted
                }
                if(makeable) break; //break the loops
            }
            foreach (var needed in eatablesNeeded)
            {
                if (makeable)
                {
                    if (eatable.EatableName == needed.EatableName) { inRecipe = true; break; } //if the inputted eatable equals one of the needed eatables
                    else inRecipe = false;
                }
                else break;
            }
        }
        else
        {
            foreach (var neededEatable in eatablesNeeded)
            {
                if (eatable?.EatableName == neededEatable?.EatableName) { inRecipe = true; break; }  //if the inputted eatable equals one of the needed eatables
                else inRecipe = false;
            }
        }
        return inRecipe;
    }

    public void CheckRecipe(List<Eatables> eatables) //check if the recipe is completed
    {
        IsMakeable = false;
        int lengthInput = eatables.Count;
        int lengthNeeded = eatablesNeeded.Count;

        if (lengthInput != lengthNeeded) { IsMakeable = false; return; } //check if the list lengths are the same

        eatables.Sort(sortByName); //sort the input list by name
        eatablesNeeded.Sort(sortByName); //sort the needed list by name

        for (int i = 0; i < eatables.Count; i++)
        {
            //Debug.Log($"input = {eatables[i].EatableName}, needed = {eatablesNeeded[i].EatableName}");
            if (eatables[i].EatableName.Equals(eatablesNeeded[i].EatableName)) { IsMakeable = true; }//check if the lists are the same if so you can make the recipe
            else IsMakeable = false;
        }
    }
}

public class SortByName : IComparer<Eatables>
{
    public int Compare(Eatables x, Eatables y)
    {
        return x.EatableName.CompareTo(y.EatableName);
    }
}