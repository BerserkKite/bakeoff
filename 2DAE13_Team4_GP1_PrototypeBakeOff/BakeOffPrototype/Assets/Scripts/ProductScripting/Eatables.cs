﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Eatables", order = 1)]
public class Eatables : ScriptableObject
{
    public string EatableName;
    public float DamageAmount;
    public float ForceAmount;
    public ThrowMechanicNames ThrowMechanic;
    public Mesh VisualObjectMesh;
    public Material VisualMaterial;
    public Color paintColor = Color.white;
    public ProductNames Name;
    [Tooltip("Number of how close he can detect when 2 or more object are near the splash")]
    public float paintDiameter = 30f;
    public float KnockbackForce = 2.5f;
    public Image Image;
}
